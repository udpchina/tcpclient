
// TCPClientHelper.cpp : implementation file
//

#include "stdafx.h"
#include "TCPClientHelper.h"
#include "CSocket2Api.h"
#include "Misc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


/*
	global variable define
*/

HANDLE	ghsockTcpip = NULL;
TCHAR	gszTcpListenIP[32] = "192.168.1.29";
INT		gnTCPListenPort = 2555;
#define RCVBUFFSIZE (1024*64)
CEdit*	gpEditEvent = NULL;
tEVENTNOTIFY	gteventnotifyTcpipSocket;

// CTCPServerDlg dialog


BOOL _setup_csocket( HWND _hwnd )
{
	BOOL	fret = 1;

	ghsockTcpip = cCreate();

	cSetupNotifyHandle( ghsockTcpip,
		_hwnd, WM_USER+124,
		NULL, NULL,
		(NOTIFYCALLBACK)_proceventnotifyTcpipSocket, &gteventnotifyTcpipSocket );
	cOpen( ghsockTcpip, NULL, 0 );

	//set remote tcp port.
	fret = cConnect( ghsockTcpip, gszTcpListenIP, gnTCPListenPort );

	return fret;
}

BOOL _endup_csocket()
{
	if( NULL == ghsockTcpip ) return FALSE;

	BOOL	fret = TRUE;

	cClose( ghsockTcpip );
	cDelete( ghsockTcpip );
	ghsockTcpip = NULL;

	return fret;
}

HRESULT CALLBACK _proceventnotifyTcpipSocket( PVOID _ptData )
{
	HRESULT hr = 0;

	tEVENTNOTIFY*			ptdata = (tEVENTNOTIFY*)_ptData;
	BYTE					acbData[1024*64] = {0,};
	DWORD					dwData = 1024*64;

	switch( ptdata->eEvent ) {
	case _FD_READ_READABLE:
		ErrDbg(1, "csock - callback _FD_READ_READABLE \r\n");

		do {
			hr = cReceivePacketFromQue( ptdata->handle, NULL, &dwData );
			hr = cReceivePacketFromQue( ptdata->handle, acbData, &dwData );
			if( NOERROR != hr ) break;

			if( 0 != memcmp( "EVENT/1.0\n", acbData, 10 ) ) break;

			gpEditEvent->LineScroll(gpEditEvent->GetLineCount());
			int len = gpEditEvent->GetWindowTextLength();
			gpEditEvent->SetSel(len,-1,true); 
			gpEditEvent->ReplaceSel((TCHAR*)acbData); 

		}while(1);
		break;
	}

	return 0;
}