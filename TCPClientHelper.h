// TCPClientHelper.h : header file
//

#pragma once
#include "CSocket2Api.h"

DWORD WINAPI TCPServer(LPVOID arg);
DWORD WINAPI ProcessClient(LPVOID arg);

extern HANDLE	ghsockTcpip;
extern TCHAR	gszTcpListenIP[32];
extern INT		gnTCPListenPort;
extern CEdit*	gpEditEvent;
extern tEVENTNOTIFY	gteventnotifyTcpipSocket;

BOOL _setup_csocket( HWND _hwnd );
BOOL _endup_csocket();
HRESULT CALLBACK _proceventnotifyTcpipSocket( PVOID _ptData );