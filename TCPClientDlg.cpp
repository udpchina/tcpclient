
// TCPClientDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TCPClient.h"
#include "TCPClientDlg.h"
#include "afxdialogex.h"
#include "TCPClientHelper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTCPClientDlg dialog




CTCPClientDlg::CTCPClientDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTCPClientDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTCPClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_EVENT, m_EditEvent);
	DDX_Control(pDX, IDC_IPADDRESS, m_IPaddr);
	DDX_Control(pDX, IDC_EDIT_PORT, m_Port);
}

BEGIN_MESSAGE_MAP(CTCPClientDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_START, &CTCPClientDlg::OnBnClickedButtonStart)
END_MESSAGE_MAP()


// CTCPClientDlg message handlers

BOOL CTCPClientDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	m_IPaddr.SetAddress(192, 168, 1, 29);
	m_Port.SetWindowText("2555");
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTCPClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTCPClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CTCPClientDlg::OnBnClickedButtonStart()
{
	// TODO: Add your control notification handler code here
	gpEditEvent = (CEdit*) &m_EditEvent;
	BYTE ipaddr[4] = {0,};
	m_IPaddr.GetAddress(ipaddr[0], ipaddr[1], ipaddr[2], ipaddr[3]);
	wsprintf( gszTcpListenIP, "%d.%d.%d.%d", ipaddr[0], ipaddr[1], ipaddr[2], ipaddr[3]);
	
	CString szPort;
	m_Port.GetWindowText(szPort);
	szPort.ReleaseBuffer();
	gnTCPListenPort = atoi( szPort.GetBuffer(szPort.GetLength()) );

	InitSocket();
	_endup_csocket();
	_setup_csocket(GetSafeHwnd());
	((CButton*)GetDlgItem(IDC_BUTTON_START))->SetWindowText("Running");
	((CButton*)GetDlgItem(IDC_BUTTON_START))->EnableWindow(FALSE);

}
